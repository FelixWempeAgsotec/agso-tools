import argparse
import pandas as pd
import os
from PyPDF2 import PdfFileMerger
import img2pdf


def getParser():
    parser = argparse.ArgumentParser("Skript to create the exepense lists for each employee.")
    parser.add_argument('folder_path', help='Path to folder containing subfolders with expenses of employees.')
    return parser

def convertImagesToPDF(folder_path):
    for file in os.listdir(folder_path):
        if os.path.splitext(file)[-1].lower() in [".jpg", ".jpeg"]:
            with open(os.path.join(folder_path, os.path.splitext(file)[0] + ".pdf"),"wb") as f:
	            f.write(img2pdf.convert(os.path.join(folder_path, file)))


if __name__ == "__main__":
    myParser = getParser()
    args = myParser.parse_args()
    folder_path = args.folder_path

    employee_data_folder = os.path.join(os.path.dirname(folder_path), '91_Vorlagen_Mitarbeiter_Auslagen')

    # First convert Jpegs to PDF.
    for folder, _, _ in os.walk(folder_path):
        convertImagesToPDF(folder)

    for folder, subfolder, folder_files in os.walk(folder_path):
        if 'Auslagen' not in folder or len(folder_files) == 0:
            continue
        assert len(subfolder) == 0, f"Folder contents for {os.path.basename(folder)} unexpected."
        # Get employee id number.
        employee_pdf = os.path.basename(folder) + '.pdf'
        employee_pdf_first_page = os.path.join(employee_data_folder, employee_pdf)

        assert os.path.isfile(employee_pdf_first_page), f"No PDF found for {os.path.basename(folder)}!"
        # Create list of PDF files.
        merge_pdf_list = [employee_pdf_first_page] + [os.path.join(folder, pdf) for
                                                        pdf in folder_files if os.path.splitext(pdf)[-1] == '.pdf']
        # Merge Excel Files.
        pdf_merger = PdfFileMerger()
        for pdf in merge_pdf_list:
            pdf_merger.append(pdf, import_bookmarks=False)
        # Save result file.
        employee_pdf_final = os.path.join(folder_path, employee_pdf)
        pdf_merger.write(employee_pdf_final)
        pdf_merger.close()
